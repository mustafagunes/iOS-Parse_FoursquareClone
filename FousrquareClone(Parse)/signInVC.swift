//
//  ViewController.swift
//  FousrquareClone(Parse)
//
//  Created by Mustafa GUNES on 27.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit
import Parse // eklendi.

class singInVC: UIViewController {

    @IBOutlet weak var usernametext: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signInClicked(_ sender: Any) {
        
        if usernametext.text != "" && passwordText.text != "" {
            
            PFUser.logInWithUsername(inBackground: usernametext.text!, password: passwordText.text!, block: { (user, error) in
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    UserDefaults.standard.set(self.usernametext.text!, forKey: "userLoggedIn")
                    UserDefaults.standard.synchronize()
                    
                    let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.rememberLogin()
                }
            })
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Username needed", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        
        if usernametext.text != "" && passwordText.text != "" {
            
            let user = PFUser()
            user.username = usernametext.text!
            user.password = passwordText.text!
            
            user.signUpInBackground(block: { (success, error) in
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    UserDefaults.standard.set(self.usernametext.text!, forKey: "userLoggedIn")
                    UserDefaults.standard.synchronize()
                    
                    let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.rememberLogin()
                }
            })
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Username needed", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

