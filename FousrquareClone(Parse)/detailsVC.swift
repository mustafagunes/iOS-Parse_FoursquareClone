//
//  detailsVC.swift
//  FousrquareClone(Parse)
//
//  Created by Mustafa GUNES on 27.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit
import Parse
import MapKit

class detailsVC: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeTypeLabel: UILabel!
    @IBOutlet weak var placeAtmosferLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var selectedPlaces = ""
    var choosenLatitude = ""
    var choosenLongitude = ""
    
    var nameArray = [String]()
    var typeArray = [String]()
    var atmosferArray = [String]()
    var latitudeArray = [String]()
    var longitudeArray = [String]()
    var imageArray = [PFFile]()
    
    var manager = CLLocationManager()
    var requestCLLocation = CLLocation()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        findPlaceFromServer()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.choosenLongitude != "" && self.choosenLatitude != "" {
            
            let location = CLLocationCoordinate2D(latitude: Double(self.choosenLatitude)!, longitude: Double(self.choosenLongitude)!)
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location, span: span)
            
            self.mapView.setRegion(region, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = self.nameArray.last!
            annotation.subtitle = self.typeArray.last!
            
            self.mapView.addAnnotation(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            
            return nil
        }
        
        let reuseID = "pin"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        
        if pinView == nil {
            
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            pinView?.canShowCallout = true
            
            let button = UIButton(type: .detailDisclosure)
            pinView?.rightCalloutAccessoryView = button
        }
        else
        {
            pinView?.annotation = annotation
        }
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if self.choosenLatitude != "" && self.choosenLongitude != "" {
            
            self.requestCLLocation = CLLocation(latitude: Double(self.choosenLatitude)!, longitude: Double(self.choosenLongitude)!)
            CLGeocoder().reverseGeocodeLocation(requestCLLocation, completionHandler: { (placemarks, error) in
                
                if let placemark = placemarks {
                    
                    if placemark.count > 0 {
                        
                        let mkPlaceMark = MKPlacemark(placemark: placemark[0])
                        
                        let mapItem = MKMapItem(placemark: mkPlaceMark)
                        mapItem.name = self.nameArray.last!
                        
                        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
                        mapItem.openInMaps(launchOptions: launchOptions)
                    }
                }
            })
        }
    }
    
    func findPlaceFromServer() {
        
        let query = PFQuery(className: "Places")
        
        query.whereKey("name", equalTo: self.selectedPlaces)
        query.findObjectsInBackground { (objects, error) in
            
            if error != nil {
                
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.nameArray.removeAll(keepingCapacity: false)
                self.typeArray.removeAll(keepingCapacity: false)
                self.atmosferArray.removeAll(keepingCapacity: false)
                self.imageArray.removeAll(keepingCapacity: false)
                self.latitudeArray.removeAll(keepingCapacity: false)
                self.longitudeArray.removeAll(keepingCapacity: false)
                
                for object in objects! {
                    
                    self.nameArray.append(object.object(forKey: "name") as! String)
                    self.typeArray.append(object.object(forKey: "type") as! String)
                    self.atmosferArray.append(object.object(forKey: "atmosfer") as! String)
                    self.imageArray.append(object.object(forKey: "image") as! PFFile)
                    self.latitudeArray.append(object.object(forKey: "latitude") as! String)
                    self.longitudeArray.append(object.object(forKey: "longitude") as! String)
                    
                    self.placeName.text = "Name: \(self.nameArray.last!)"
                    self.placeTypeLabel.text = "Type: \(self.typeArray.last!)"
                    self.placeAtmosferLabel.text = "Atmosfer: \(self.atmosferArray.last!)"
                    self.choosenLatitude = self.latitudeArray.last!
                    self.choosenLongitude = self.longitudeArray.last!
                    
                    self.manager.startUpdatingLocation()
                    
                    self.imageArray.last?.getDataInBackground(block: { (data, error) in
                        
                        if error != nil {
                            
                            let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                            
                            alert.addAction(okButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.placeImage.image = UIImage(data: data!)
                        }
                    })
                }
            }
        }
    }
}