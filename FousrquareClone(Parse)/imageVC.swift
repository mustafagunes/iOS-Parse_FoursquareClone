//
//  imageVC.swift
//  FousrquareClone(Parse)
//
//  Created by Mustafa GUNES on 27.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit

//  Global değişkenler

    var globalName = ""
    var globalType = ""
    var globalAtmosfer = ""
    var globalImage = UIImage()

// -----------------------------------

class imageVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate { // eklendi.

    @IBOutlet weak var placesNameText: UITextField!
    @IBOutlet weak var placeTypeText: UITextField!
    @IBOutlet weak var atmosferText: UITextField!
    @IBOutlet weak var placeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesturesRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageVC.selectImage))
        placeImage.addGestureRecognizer(gesturesRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        globalName = ""
        globalType = ""
        globalAtmosfer = ""
        globalImage = UIImage()
    }
    
    @objc func selectImage() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        placeImage.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        
        if placesNameText.text != "" && placeTypeText.text != "" && atmosferText.text != "" {
            
            if let choosenImage = placeImage.image {
                
                globalName = placesNameText.text!
                globalType = placeTypeText.text!
                globalAtmosfer = atmosferText.text!
                globalImage = choosenImage
            }
        }
        
        self.performSegue(withIdentifier: "fromimageVCtomapVC", sender: nil)
        
        placesNameText.text = ""
        placeTypeText.text = ""
        atmosferText.text = ""
        placeImage.image = UIImage(named: "select.png")
    }
}