//
//  placesVC.swift
//  FousrquareClone(Parse)
//
//  Created by Mustafa GUNES on 27.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit
import Parse

class placesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var placesNameArray = [String]()
    var choosenPlace = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        getData()
    }
    
    func getData() { // PARSEDEN VERİLERİ ÇEKİLECEK FONSİYON !
        
        let query = PFQuery(className: "Places")
        
        query.findObjectsInBackground { (objects, error) in
            
            if error != nil {
                
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                for object in objects! {
                    
                    self.placesNameArray.removeAll(keepingCapacity: false)
                    self.placesNameArray.append(object.object(forKey: "name") as! String)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "fromplacesVCtodetailsVC" {
            
            let destinationVC = segue.destination as! detailsVC
            destinationVC.selectedPlaces = self.choosenPlace
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.choosenPlace = placesNameArray[indexPath.row]
        self.performSegue(withIdentifier: "fromplacesVCtodetailsVC", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = placesNameArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return placesNameArray.count
    }
    
    @IBAction func logoutClicked(_ sender: Any) {
        
        UserDefaults.standard.removeObject(forKey: "userLoggedIn")
        UserDefaults.standard.synchronize()
        
        let signInVC = self.storyboard?.instantiateViewController(withIdentifier: "signInVC") as! singInVC
        let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        delegate.window?.rootViewController = signInVC
        delegate.rememberLogin()
    }
    @IBAction func addClicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "fromplacesVCtoimageVC", sender: nil)
    }
}
